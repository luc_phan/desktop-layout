import pywinauto
from pywinauto import Desktop, Application
import pprint


def main():
    top_windows = Desktop(backend="uia").windows()  # or backend="win32" by default

    # walk the returned list of wrappers
    for i, w in enumerate(top_windows):
        if not w.is_dialog():
            continue
        print(
            i,
            w.window_text(),
            # w.is_visible(),
            # w.is_active(),
            # w.is_dialog(),
            # w.get_show_state(),
            # w.is_keyboard_focusable(),
            # w.class_name(),
            # w.element_info,
            # w.friendly_class_name(),
            # w.get_properties(),
            # w.is_enabled(),
            # w.process_id()
        )
        # print(type(w))
        # print(dir(w))
        # pprint.pprint(w.get_properties())

        # app = Application().connect(process=w.process_id())
        # print(app)
        # print(dir(app))
        # for attribute in dir(app):
        #     print('-', attribute, getattr(app, attribute))

        # print(pywinauto.application.process_get_modules())
        for pid, exe_path, _ in pywinauto.application.process_get_modules():
            if pid != w.process_id():
                continue
            print(pid, exe_path, _)
