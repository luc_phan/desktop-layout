from setuptools import setup

setup(
    name='desktop-layout',
    version='0.0.1',
    packages=[''],
    url='',
    license='',
    author='',
    author_email='',
    description='',
    entry_points={'console_scripts': ['desktop-layout = desktoplayout.command:main']},
    install_requires=['pywinauto']
)
