desktop-layout
==============

Objective
---------

```yaml
---
video:
  - predicates:
      exe_path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
      title_re: ^Netflix - Google Chrome
main:
  - predicates:
      exe_path: C:\Program Files\JetBrains\PyCharm Community Edition 2019.3.4\bin\pycharm64.exe
  - predicates:
      exe_path: C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
      title_re: "- Google Chrome"
fixed:
  - predicates:
      exe_path: C:\Program Files\Double Commander\doublecmd.exe
    position:
      x: 655
      y: 520
      width: 1272
      height: 527
  - predicates:
      exe_path: C:\cygwin64\bin\mintty.exe
    position:
      x: -7
      y: 646
      width: 675
      height: 401
...
```

```
>desktop-layout
```

Packages
--------

### Command-line parsing

- argparse
- docopt
- click

### Windows automation

- pywinauto
- pyautogui (screenshots)
